package co.runloop.bleserver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.nio.ByteBuffer;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final UUID ADVERTISE_SERVICE_DATA_UUID = UUID.fromString("20b9045a-bd3f-48f2-88e8-62737e083922");


    private static final UUID VEHICLE_STATE_SERVICE_UUID = UUID.fromString("eea340ac-2bb9-4bea-9b5f-9d905d1cc42f");
    private static final UUID VEHICLE_STATE_CHARACTERISTIC_UUID = UUID.fromString("43b4df89-8807-4edb-b785-6e97fdc0a769");

    private static final UUID AUTH_SERVICE_UUID = UUID.fromString("59023e68-62ca-4eb0-b36f-c4def6bc1ce9");
    private static final UUID AUTH_STATE_CHARACTERISTIC_UUID = UUID.fromString("acea06c5-9139-4a4d-9723-3c548ee41bbf");
    private static final UUID AUTH_PASSWORD_CHARACTERISTIC_UUID = UUID.fromString("310b524f-3737-4a33-9a62-d5e5b55a6786");
    private static final String DEFAULT_AUTH_PASSWORD = "12345";

    private String deviceString(BluetoothDevice device) {
        return "\"" + device.getName() + "\" - \"" + device.getAddress() + "\"";
    }

    private BluetoothGattServerCallback gattServerCallback = new BluetoothGattServerCallback() {
        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i(TAG, "new device connected, device: " + deviceString(device));
                    clientDeviceInfo = new DeviceInfo(device);
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.i(TAG, "device disconnected, device: " + deviceString(device));
                    clientDeviceInfo = null;
                    break;
            }
        }

        @Override
        public void onServiceAdded(int status, BluetoothGattService service) {
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device,
                                                int requestId,
                                                int offset,
                                                BluetoothGattCharacteristic characteristic) {
            Log.i(TAG, "onCharacteristicReadRequest, device: " + deviceString(device) + "\n characteristic uuid: " + characteristic.getUuid());
            if (characteristic.getUuid().equals(AUTH_STATE_CHARACTERISTIC_UUID)) {

            } else if (characteristic.getUuid().equals(VEHICLE_STATE_CHARACTERISTIC_UUID)) {
                byte[] val = ByteBuffer.allocate(1).put((byte) (isVehicleLocked ? 1 : 0)).array();
                bluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, val);
            }
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device,
                                                 int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {
            Log.i(TAG, "onCharacteristicWriteRequest, device: " + deviceString(device) + "\n characteristic uuid: " + characteristic.getUuid());
            if (characteristic.getUuid().equals(AUTH_PASSWORD_CHARACTERISTIC_UUID)) {
                String password = new String(value);
                Log.i(TAG, "password: " + password);
                if (DEFAULT_AUTH_PASSWORD.equals(password)) {
                    Log.i(TAG, "auth successful");
                    clientDeviceInfo.setAuthenticated(true);
                    BluetoothGattCharacteristic authCharacteristic = authStateService.getCharacteristic(AUTH_STATE_CHARACTERISTIC_UUID);
                    authCharacteristic.setValue(1, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    bluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value);
                    bluetoothGattServer.notifyCharacteristicChanged(device, authCharacteristic, false);
                } else {
                    Log.i(TAG, "auth fail, wrong password");
                    bluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, offset, value);
                }
            } else if (characteristic.getUuid().equals(VEHICLE_STATE_CHARACTERISTIC_UUID)) {
                if (clientDeviceInfo.isAuthenticated()) {
                    Log.d(TAG, "val length: " + value.length);
                    int isLockedInt = value[0];
                    characteristic.setValue(isLockedInt, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    isVehicleLocked = isLockedInt == 1;
                    Log.i(TAG, "change vehicle lock state to: " + isVehicleLocked);
                    bluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value);
                    bluetoothGattServer.notifyCharacteristicChanged(clientDeviceInfo.getDevice(), characteristic, false);
                } else {
                    bluetoothGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, offset, value);
                }
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device,
                                            int requestId,
                                            int offset,
                                            BluetoothGattDescriptor descriptor) {
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device,
                                             int requestId,
                                             BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite,
                                             boolean responseNeeded,
                                             int offset,
                                             byte[] value) {
        }

        @Override
        public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
        }

        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            Log.i(TAG, "onNotificationSent to: " + deviceString(device));
        }

        @Override
        public void onMtuChanged(BluetoothDevice device, int mtu) {
        }

        @Override
        public void onPhyUpdate(BluetoothDevice device, int txPhy, int rxPhy, int status) {
        }

        @Override
        public void onPhyRead(BluetoothDevice device, int txPhy, int rxPhy, int status) {
        }
    };
    private ToggleButton serverBtn;
    private TextView infoTv;
    private CheckBox isEncryptedCheckBox;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeAdvertiser bluetoothLeAdvertiser;
    private BluetoothGattServer bluetoothGattServer;
    private AdvertiseCallback advertiseCallback;

    private DeviceInfo clientDeviceInfo;
    private BluetoothGattService authStateService;
    private BluetoothGattService vehicleStateService;

    private boolean isVehicleLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        advertiseCallback = new AdvertiseCallback() {
            @Override
            public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                Log.i(TAG, "Advertisement started successful, settings: " + settingsInEffect.toString());
            }

            @Override
            public void onStartFailure(int errorCode) {
                Log.i(TAG, "Advertisement start error, code: " + errorCode);
            }
        };
        serverBtn = findViewById(R.id.start_server_togglebtn);
        serverBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startServer(isEncryptedCheckBox.isChecked());
                    startAdvertisement();
                } else {
                    stopServer();
                    stopAdvertisement();
                }
            }
        });
        infoTv = findViewById(R.id.info_tv);
        isEncryptedCheckBox = findViewById(R.id.encrypted_checkbox);

        bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

    }


    private void startServer(boolean encrypted) {
        bluetoothGattServer = bluetoothManager.openGattServer(this, gattServerCallback);
        vehicleStateService = createVehicleStateService(encrypted);
        bluetoothGattServer.addService(vehicleStateService);
        authStateService = createAuthService(encrypted);
        bluetoothGattServer.addService(authStateService);
    }

    private void startAdvertisement() {
        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();
        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(false)
                //.addServiceUuid(new ParcelUuid(ADVERTISE_SERVICE_DATA_UUID))
                //.addServiceData(new ParcelUuid(ADVERTISE_SERVICE_DATA_UUID), "VehicleState".getBytes())
                .build();
        bluetoothLeAdvertiser = bluetoothAdapter.getBluetoothLeAdvertiser();
        bluetoothLeAdvertiser.startAdvertising(settings, data, advertiseCallback);
    }

    private void stopAdvertisement() {
        if (bluetoothLeAdvertiser != null) {
            bluetoothLeAdvertiser.stopAdvertising(advertiseCallback);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopServer();
        stopAdvertisement();
    }

    private void stopServer() {
        if (bluetoothGattServer != null) {
            bluetoothGattServer.close();
            bluetoothGattServer = null;
        }
    }

    private BluetoothGattService createAuthService(boolean encrypted) {
        BluetoothGattService service = new BluetoothGattService(AUTH_SERVICE_UUID, BluetoothGattService.SERVICE_TYPE_PRIMARY);

        int properties = BluetoothGattCharacteristic.PROPERTY_WRITE
                | BluetoothGattCharacteristic.PROPERTY_NOTIFY
                | BluetoothGattCharacteristic.PROPERTY_READ;
        int perms;
        if (encrypted) {
            perms = BluetoothGattCharacteristic.PERMISSION_WRITE_ENCRYPTED_MITM;
        } else {
            perms = BluetoothGattCharacteristic.PERMISSION_WRITE;
        }
        BluetoothGattCharacteristic authStateCharacteristic = new BluetoothGattCharacteristic(
                AUTH_STATE_CHARACTERISTIC_UUID,
                properties,
                perms);
        service.addCharacteristic(authStateCharacteristic);

        properties = BluetoothGattCharacteristic.PROPERTY_READ
                | BluetoothGattCharacteristic.PROPERTY_WRITE;
        perms = BluetoothGattCharacteristic.PERMISSION_WRITE
                | BluetoothGattCharacteristic.PERMISSION_READ;
        BluetoothGattCharacteristic passwordCharacteristic = new BluetoothGattCharacteristic(
                AUTH_PASSWORD_CHARACTERISTIC_UUID,
                properties,
                perms);
        service.addCharacteristic(passwordCharacteristic);

        return service;
    }

    private BluetoothGattService createVehicleStateService(boolean encrypted) {
        BluetoothGattService service = new BluetoothGattService(VEHICLE_STATE_SERVICE_UUID, BluetoothGattService.SERVICE_TYPE_PRIMARY);

        int properties = BluetoothGattCharacteristic.PROPERTY_WRITE
                | BluetoothGattCharacteristic.PROPERTY_READ
                | BluetoothGattCharacteristic.PROPERTY_NOTIFY;
        int perms;
        if (encrypted) {
            perms = BluetoothGattCharacteristic.PERMISSION_WRITE_ENCRYPTED_MITM
                    | BluetoothGattCharacteristic.PERMISSION_READ_ENCRYPTED_MITM;
        } else {
            perms = BluetoothGattCharacteristic.PERMISSION_WRITE
                    | BluetoothGattCharacteristic.PERMISSION_READ;
        }
        BluetoothGattCharacteristic vehicleStateCharacteristic = new BluetoothGattCharacteristic(
                VEHICLE_STATE_CHARACTERISTIC_UUID,
                properties,
                perms);

        Log.d(TAG, "add vehicleStateCharacteristic: " + service.addCharacteristic(vehicleStateCharacteristic));

        return service;
    }
}
