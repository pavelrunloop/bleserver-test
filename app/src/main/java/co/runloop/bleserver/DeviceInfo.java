package co.runloop.bleserver;

import android.bluetooth.BluetoothDevice;

public class DeviceInfo {

    private boolean authenticated;
    private final BluetoothDevice device;

    public DeviceInfo(BluetoothDevice device) {
        this.device = device;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public BluetoothDevice getDevice() {
        return device;
    }
}
